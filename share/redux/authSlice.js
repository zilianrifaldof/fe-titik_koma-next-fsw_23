import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
    name: "auth",
    initialState: {
    isLoggedIn: false,
    token: null,
    },
    name: "stateModal", // State modal create game untuk diakses disetiap komponen jika diperlukan
    initialState: {
    isClicked: false,
    },
    name: "singeModal",
    initialState: {
      isOpen: false,
    },
    reducers: {
        // reducer to update state to loggin is true and add jwt token
        setLogin: (state, action) => {
          state.isLoggedIn = true;
          state.token = action.payload;
        },
        // reducer to update state to loggin is false and remove jwt
        setLogout: (state, action) => {
          state.isLoggedIn = false;
          state.token = "udah logout";
        },
        setSinglePlayerTrue: (state,action) => {
          state.isOpen = true;
        },
        setSinglePlayerFalse: (state,action) => {
          state.isOpen = false;
        },
        setModalRoomClickedTrue: (state, action) => {
          state.isClicked = true;
      },
        setModalRoomClickedFalse: (state, action) => {
        state.isClicked = false;
  },
    }
});

export const { setLogin, setLogout, setSinglePlayerTrue, setSinglePlayerFalse, setModalRoomClickedTrue, setModalRoomClickedFalse} = authSlice.actions;
export default authSlice.reducer;