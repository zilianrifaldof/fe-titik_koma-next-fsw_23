import style from "../styles/Dashboard.module.css";
import "bootstrap/dist/css/bootstrap.css";

import React, {useEffect, useState} from "react";
import CreateRoom from "./CreateRoom/CreateRoom";
import { useDispatch, useSelector } from "react-redux";
import { setModalRoomClickedTrue } from "../share/redux/authSlice";
import { useRouter } from "next/router";
import style2 from "../styles/Loading.module.css"
import Pusher from "pusher-js";
import axios from "axios";

const RoomList = (props) => {
  const dispatch = useDispatch();
  const stateModal = useSelector((state) => state.stateModal);
  const gameLists = props.gameListsProps;
  const router = useRouter();

  const [startPage, setStartPage] = useState(0)
  const [limitPage, setLimitPage] = useState(8)

  const [pageNumber, setPageNumber] = useState(1)

  const handleClick = (e) => {
    router.push(`games/rps/${e.target.value}`)
  }

  const reloadClicked = () => {
    setLimitPage(8)
    setStartPage(0)
    setPageNumber(1)
    router.push(RoomList,undefined,{ scroll: false})
  }

  const setCreateGameFunction = () => {
    dispatch(setModalRoomClickedTrue());
  };
  const onClickNext = (startPage,limitPage) => {
    console.log("masuk", startPage)

      console.log("masuk")
      setStartPage(startPage+9)
      setLimitPage(limitPage+9)
      setPageNumber(pageNumber+1)
    
  };

  const onClickPrevious = (startPage,limitPage) => {
    console.log("back", startPage)
    if(startPage>=1){
      setStartPage(startPage-9)
      setLimitPage(limitPage-9)
      setPageNumber(pageNumber-1)


    }
  };

  
  return (
    <>
  
      <div className="row mt-5 section_3">
      <div className={style.btn_create_room} >
            <button
              className="btn btn-danger btn-sm"
              onClick={() => setCreateGameFunction()}
            >
              Create Own Room
            </button>
          </div>
        <div className={style.quick_play}>
          <i class="bi bi-caret-right-fill"></i>
          <span>Room List</span>
          <span> - </span>
          <span className={style.section_3_vs_player_text}>Vs Player</span>
      <button className="btn btn-sm btn-dark mx-3" onClick={reloadClicked}> Update Room</button>
        </div>
        
        {gameLists.slice(startPage, limitPage).map((gameList) => {
          return (
            <div key={gameList.id} className="col-md-6 mb-3">
              <div className={style.quick_play_box}>
                <div className={style.card_box}>
                  <h5 className="justify-content-center text-center">
                    {gameList.name}
                  </h5>
                  <div>
                    {gameList.winner}
                  </div>
                  <button value={gameList.id} onClick={handleClick} className="btn btn-sm btn-danger">Join Now</button>
                </div>
              </div>
            </div>
          );
        })}
        <div className={style.btn_room_list_wrapper}>
        <div className={style.pagination_room_list}>
        <div aria-label="Page navigation example" >
              <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#"onClick={() => {
            // onClickNext(parseInt(currentPage) - 1, 1);
            onClickPrevious(startPage,limitPage);
          }}>
                    <i class="bi bi-chevron-double-left"></i>
                  </a>
                </li>
                {pageNumber}
                <li class="page-item">

              
                  <a class="page-link" href="#" onClick={() => {
            onClickNext(startPage,limitPage);
          }}>
                 
                    <i class="bi bi-chevron-double-right"   ></i>
                  </a>
                </li>
              </ul>
        
            </div>  </div>
          {/* <div className={style.pagination_room_list}>
          <div aria-label="Page navigation example" >
              <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#" >
                    <i class="bi bi-chevron-double-left"></i>
                  </a>
                </li>
            
                <li class="page-item">

              
                  <a class="page-link" href="#">
                 
                    <i class="bi bi-chevron-double-right"   ></i>
                  </a>
                </li>
              </ul>
            </div>
          </div> */}
          {/* <div className={style.btn_room_list}>
                <button className='btn btn-dark btn-sm'>See More Room</button>
            </div> */}
          
      
          <CreateRoom />
            
        </div>
      </div>
    </>
  );
};

export default RoomList;
