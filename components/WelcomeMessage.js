import axios from "axios";
import { useState, useEffect } from "react";
import { getCookie, getCookies } from 'cookies-next';
import style from "../styles/Dashboard.module.css";

const WelcomeMessage = () => {
  const cookies = getCookies();
    const [users, setUsers] = useState({});
    const weekList = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];
    const monthList = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
  ];
  const current = new Date();
  let day = weekList[current.getDay()];
  let month = monthList[current.getMonth()];
  const date = `${current.getDate()} ${month} ${current.getFullYear()}`;

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "https://games-website-titikkoma-be-dev.herokuapp.com") +
          `/user/${cookies.userId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <div className={style.welcome_message}>
        <div className={style.say_hello +" "+ "text-capitalize"}>Hello, {users.fullname}</div>
        <div className={style.today_date}>Today is {day}, {date}</div>
    </div>
  )
}

export default WelcomeMessage;
