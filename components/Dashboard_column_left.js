import axios from 'axios';
import Link from 'next/link';
import style from '../styles/Dashboard.module.css';
import { useRouter } from 'next/router';
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useState, useEffect } from "react";

const Dashboard_column_left = (props) => {
  const cookies = getCookies();
  const [users, setUsers] = useState({});
  const router = useRouter(); 

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "https://games-website-titikkoma-be-dev.herokuapp.com") +
          `/user/${cookies.userId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
      <div className={style.column_left +" "+ "col-md-2"}>
        <div className={style.profile_logo}>Titik Koma</div>
          <div className={style.column_left_section1}>
            <div className={style.profile_pic}></div>
            <div className={style.profile_name}>{users.fullname}</div>
            <div className={style.profile_email}>{users.email}</div>
          </div>
          <div className={style.column_left_section2}>
            <nav className="nav flex-column">
              <li className="nav-item">
                <Link href="/dashboard">
                  <a className={router.pathname == "/dashboard" ? `${style.active} nav-link` : "nav-link"} aria-current="page">
                    <i className="bi bi-house-door-fill"></i>
                    <span className='ms-3'>Dashboard</span>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/profile">
                  <a className={router.pathname == "/profile" ? `${style.active} nav-link` : "nav-link"}>
                    <i className="bi bi-person-circle"></i>
                    <span className='ms-3'>Profile</span>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/password">
                  <a className={router.pathname == "/password" ? `${style.active} nav-link` : "nav-link"}>
                    <i className="bi bi-shield-lock-fill"></i>
                    <span className='ms-3'>Password</span>
                  </a>
                </Link>
              </li>
            </nav>
            </div>
        </div>
  )
}

export default Dashboard_column_left;