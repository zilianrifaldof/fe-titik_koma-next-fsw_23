import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import styles from "../../styles/Home.module.css";

export const Footer = () => {
  return (
    <>
      <br />
      <div className="footer">
          <div className="footerContent">© 2022 - Titik Koma</div>
      </div>
    </>
  );
};

export default Footer;
