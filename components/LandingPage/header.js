import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Link from "next/link";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { setLogin, setLogout } from "../../share/redux/authSlice";
import { useRouter } from "next/router";
import Head from "next/head";
import Image from "next/image";

export const Header = () => {
  const cookies = getCookies();
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const router = useRouter();

  useEffect(() => {
    if (cookies.accessToken !== "undefined" && cookies.accessToken) {
      dispatch(setLogin(cookies.accessToken));
    }
  }, []);

  const handleLogout = () => {
    const text = "are you sure want to logout?";
    if (window.confirm(text) === true) {
      deleteCookie(["accessToken"]);
      deleteCookie(["userId"]);
      router.replace("/login");
      dispatch(setLogout());
    }
  };

  let menu;
  if (!auth.isLoggedIn) {
    menu = (
      <>
        <Link href="/login">
          <Navbar.Brand style={{ cursor: "pointer" }}>Login</Navbar.Brand>
        </Link>
        <Link href="/registration">
          <Navbar.Brand style={{ cursor: "pointer" }}>
            Registration
          </Navbar.Brand>
        </Link>
      </>
    );
  } else {
    menu = (
      <>
        <Link href="/dashboard">
          <Navbar.Brand style={{ cursor: "pointer" }}>
            Dashboard
          </Navbar.Brand>
        </Link>
        {/* <Link href="/"> */}
          <Navbar.Brand style={{ cursor: "pointer" }} onClick={handleLogout}>
            Logout
          </Navbar.Brand>
        {/* </Link> */}
      </>
    );
  }

  return (
    <>
      <Navbar style={{borderBottom:"1px solid #C3C3C3",background:"#E8E8E8"}} sticky="top" className="my-0" color="secondary" dark="true">
        <Container>
          <Link href="/">
            <Navbar.Brand style={{ cursor: "pointer" }}>
              <img src="/logo.png" alt="titik-koma" style={{height: "30px"}}></img>
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            {menu}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
