import axios from 'axios';
import { useRouter } from 'next/router';
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useState, useEffect } from "react";
import style from '../styles/Dashboard.module.css'

const Dashboard_column_right = (props) => {
  const [leaders, setLeaders] = useState([]);
  const cookies = getCookies();
  const [users, setUsers] = useState({});
  const router = useRouter();

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "https://games-website-titikkoma-be-dev.herokuapp.com") +
          `/user/${cookies.userId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  const fetchLeaders = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL ||
          "https://games-website-titikkoma-be-dev.herokuapp.com") +
          "/users?pageNumber=1&limitParm=5"
      )
      .then((res) => {
        const listLeaders = res.data;
        setLeaders(listLeaders);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    fetchLeaders();
  }, []);

  return (
    <div className={style.column_right +" "+ "col-md-3"}>
        <div className={style.column_right_section_1}>
            <div className={style.my_score_title}>My score</div>
            <div className={style.my_score_box}>{users.score}</div>
        </div>
        <div className={style.column_right_section_2}>
          <div className={style.leaderboard_title}><i className="bi bi-trophy-fill text-danger">
            </i><span className='ms-3'>Leaderboard</span>
          </div>
          <div className={"row" +" "+ style.leaderboard_box}>
            <div className={'col-md-2 text-center' +" "+ style.leaderboard_column_title}>#</div>
            <div className={'col-md-7' +" "+ style.leaderboard_column_title}>Name</div>
            <div className={'col-md-3' +" "+ style.leaderboard_column_title}>Score</div>
          {leaders.map((leader, index) => {
            return (
              <>
                <div className='col-md-2 text-center fw-bold'>
                  { index + 1 }
                </div>
                <div className='col-md-7 text-capitalize'>
                  { leader.fullname }
                </div>
                <div className='col-md-3'>
                  { leader.score }
                </div>
              </>
              );
            })}
          </div>
        </div>
    </div>
  )
}

export default Dashboard_column_right;
