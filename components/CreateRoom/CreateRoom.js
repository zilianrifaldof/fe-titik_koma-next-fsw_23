import style from "../../styles/Dashboard.module.css";
import "bootstrap/dist/css/bootstrap.css";
import { useRouter } from 'next/router';
import React, { useState } from "react";
import axios from "axios";
import { Button, Modal, Row, Col, Form } from "reactstrap";
import { getCookies } from 'cookies-next';
import { useDispatch, useSelector } from "react-redux";
import RoomList from "../RoomList";
import style2 from "../../styles/Loading.module.css"

import {
  setModalRoomClickedFalse,
  setModalRoomClickedTrue,
} from "../../share/redux/authSlice";


const CreateRoom = () => {
  //POP UP CREATE GAME/ROOM (STATE GLOBAL REDUX)
  const [successCreated, setsuccesscreated] = useState(false);
  const [failedCreated, setFailedCreated] = useState(false);
  const [createLot, setCreateLot] = useState(true);
  const [valuecreateLot, setvalueCreateLot] = useState(false);
  const [roomData, setRoomData] = useState({ name: null, description: null });

  const [onlyCreateButton, setOnlyCreateButton] = useState(false);
  const [loadingPlay, setLoadingPlay] = useState(false);
  const dispatch = useDispatch();
  const stateModal = useSelector((state) => state.stateModal);
  const cookies = getCookies();
  const router = useRouter();



  const handleChange = (choice) => {
    if (choice === "createLot") {
      {
        if (createLot === true) {
          setCreateLot(false);
          return setvalueCreateLot(true);
        }
        setvalueCreateLot(false);
        return setCreateLot(!createLot);
      }
    }
  };
  const handleChangeRoomData = (e) => {
    setRoomData({ ...roomData, [e.target.name]: e.target.value });
    console.log(roomData);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post(
        "https://games-website-titikkoma-be-dev.herokuapp.com" +
          "/game/createGame",
        roomData,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        console.log("lewat barer");
        const roomId = res.data.id;

        if (res.status === 200 && onlyCreateButton === true) {
          setLoadingPlay(false)
          setOnlyCreateButton(false)
          setsuccesscreated(true);
          setFailedCreated(false)
          const timer = setTimeout(() => {
            setsuccesscreated(false);
          
            if (valuecreateLot === false) {
              const timer2 = setTimeout(() => {
                dispatch(setModalRoomClickedFalse());
              }, 200);
              return () => clearTimeout(timer2);
            }
          }, 800);

          if (valuecreateLot === false) {
            setRoomData({ name: null, description: null });
          }
          console.log("modal diklik ", stateModal.isClicked)

          router.push(RoomList,undefined,{ scroll: false})
          
          return () => clearTimeout(timer);
        }
        else if(res.status === 200){
          setLoadingPlay(false)
          dispatch(setModalRoomClickedFalse());
          router.push(`games/rps/${roomId}`)
        }
      })
      .catch((err) => {
        setLoadingPlay(false)
        const { request } = err;
        setOnlyCreateButton(false)
        if(roomData.description && roomData.name){
        setsuccesscreated(false);
        setFailedCreated(true)
        dispatch(setModalRoomClickedTrue());
   
        const timer = setTimeout(() => {
          setsuccesscreated(false);
          setFailedCreated(false)
        }, 800);
        setRoomData({ name: null, description: null });
        return () => clearTimeout(timer);
      }
      else{
        setFailedCreated(true)
        setsuccesscreated(false);
        const timer = setTimeout(() => {
          setsuccesscreated(false);
          setFailedCreated(false)
        }, 800);
        return () => clearTimeout(timer);
      }
      });
      
  };
  

  return (
    <>
      <main>
        <Row>
          <Col md="2">
            <Modal
              isOpen={stateModal.isClicked}
              toggle={() => {
                dispatch(setModalRoomClickedFalse()),
                  setvalueCreateLot(false),
                  setCreateLot(true),
                  setFailedCreated(false),
                  setRoomData({ name: null, description: null });
              }}
            >
              <div className=" modal-header">
                <h6
                  className="className={style.btn_room_list}"
                  id="modal-title-default"
                >
                  Create Own Room
                </h6>
            
              </div>
              <div>
                <form onSubmit={handleSubmit}>
                  <div className=" modal-body">
                    <div className="form-group">
                      <label for="exampleInputName1">Room Name</label>
                      <input
                        style={{ width: "90%" }}
                        type="text"
                        className="form-control"
                        id="exampleInputName1"
                        aria-describedby="emailHelp"
                        name="name"
                        placeholder="My room"
                        onChange={handleChangeRoomData}
                      ></input>
                      <small
                        id="emailHelp"
                        className="form-text text-muted"
                      ></small>
                    </div>
                    <div className="form-group">
                      <label for="exampleInputDescription1">Description</label>
                      <input
                        style={{ width: "90%" }}
                        type="text"
                        name="description"
                        className="form-control"
                        id="exampleInputDescription1"
                        placeholder="My room description"
                        onChange={handleChangeRoomData}
                      ></input>
                    </div>
                    <div style={{ marginTop: "8px" }}>
                      <div class="form-check">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          value={createLot}
                          onChange={() => handleChange("createLot")}
                          id="flexCheckDefault"
                        ></input>
                        <label
                          class="form-check-label"
                          for="flexCheckDefault"
                          style={{ fontSize: "13.5px" }}
                        >
                          create multiple rooms
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className=" modal-footer">
                    <div>
                      {successCreated === true ? (
                        <div
                          style={{
                            marginLeft: "-130px",
                            width: "20px",
                            height: "20px",
                          }}
                          class="spinner-grow text-primary 1"
                          role="status"
                        >
                          <span
                            class="sr-only"
                            style={{ marginLeft: "30px", fontSize: "14px" }}
                          >
                            {" "}
                            successfully {" "}
                          </span>
                        </div>
                      ) : failedCreated === true ? <> <div   style={{
                            marginLeft: "-130px",
                           
                            width: "18px",
                            height: "18px",
                          }}class="spinner-grow text-danger" role="status">
                      <span class="sr-only" style={{ marginLeft: "30px", fontSize: "18px" }}> Failed</span>
                    </div> </> : (
                        <></>
                      )}
                      { loadingPlay === true ? 
                      <div>
                        <div className={style2.loadingStyle}><div  className={style2.loadingStyle}>
                        </div><div  className={style2.loadingStyle}>
                        </div><div className={style2.loadingStyle}></div>
                        <div className={style2.loadingStyle}></div></div> </div> :  <></>     
                      }   
                    </div>
                      
                  
                    {/* {
                      createLot === true ? */}
                      
                    <Button color="primary" type="submit" onClick={() => setLoadingPlay(true)}>
                      
                      Ok, let's play
                    </Button>
                      
                    <Button color="success" type="submit" onClick={() => {setOnlyCreateButton(true),setLoadingPlay(true)}}>
                      Ok, only create
                    </Button>
                    {/* } */}

                    <Button
                      className=" ml-auto"
                      style={{ backgroundColor: "black", color: "white" }}
                      onClick={() => {
                        dispatch(setModalRoomClickedFalse()),
                          setvalueCreateLot(false),
                          setCreateLot(true),
                          setFailedCreated(false),
                          setRoomData({ name: null, description: null });

                      }}
                      type="button"
                    >
                      Cancel
                    </Button>
                  </div>
                </form>
              </div>
            </Modal>
          </Col>
        </Row>
      </main>
    </>
  );
};

export default CreateRoom;
