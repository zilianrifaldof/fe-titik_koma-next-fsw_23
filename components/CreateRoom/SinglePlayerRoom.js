import style from "../../styles/Dashboard.module.css";
import "bootstrap/dist/css/bootstrap.css";

import React, { useState } from "react";
import { Button, Modal, Row, Col, Form } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";

import { setSinglePlayerFalse } from "../../share/redux/authSlice";
import { Router, useRouter } from "next/router";

const SingleRoom = (props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const stateModal = useSelector((state) => state.stateModal);
  const [gameType, setGameType] = useState(null);

  const handleChange = (e) => {
    e.preventDefault();
    setGameType(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (gameType !== "" && gameType !== null) {
      router.push(`games/${gameType}`);
      dispatch(setSinglePlayerFalse())
    } else {
      alert('Please choose a valid option.')
    }
  };

  return (
    <>
      <main>
        <Row>
          <Col md="2">
            <Modal
              isOpen={stateModal.isOpen}
              toggle={() => {
                dispatch(setSinglePlayerFalse());
              }}
            >
              <div className=" modal-header">
                <h6 id="modal-title-default">Single Player Game</h6>
              </div>
              <div>
                <form onSubmit={handleSubmit}>
                  <div className=" modal-body">
                    <div>Choose your game</div>
                    <div className="my-2">
                      <select onChange={handleChange} style={{ width: "90%" }}>
                        <option value="">------------------</option>
                        <option value="rps">Rock Paper Scissor</option>
                        <option value="hangman">Hangman</option>
                        <option value="number">Number Guessing</option>
                      </select>
                    </div>
                  </div>

                  <div className=" modal-footer">
                    <Button color="success" type="submit">
                      Play Game
                    </Button>

                    <Button
                      className=" ml-auto"
                      style={{ backgroundColor: "black", color: "white" }}
                      onClick={() => {
                        dispatch(setSinglePlayerFalse());
                      }}
                      type="button"
                    >
                      Cancel
                    </Button>
                  </div>
                </form>
              </div>
            </Modal>
          </Col>
        </Row>
      </main>
    </>
  );
};

export default SingleRoom;
