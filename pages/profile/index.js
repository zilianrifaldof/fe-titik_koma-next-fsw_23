import axios from "axios";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useState, useEffect } from "react";
import { useRouter } from 'next/router';
import style from '../../styles/Dashboard.module.css';
import Dashboard_column_left from '../../components/Dashboard_column_left';
import Dashboard_column_right from '../../components/Dashboard_column_right';
import profileStyle from '../../styles/Profile.module.css';
import WelcomeMessage from "../../components/WelcomeMessage";

const Index = (props) => {
  const cookies = getCookies()
  const [users, setUsers] = useState({});
  const [values, setValues] = useState({});
  const [message, setMessage] = useState(false);
  console.log(message);
  const router = useRouter();

  const enableForm = () => {
    const inputs = document.querySelectorAll("input");
    const buttonSubmit = document.getElementById("submit");
    const buttonEdit = document.getElementById("edit");
    const messageDom = document.getElementById("message");
    inputs[0].removeAttribute("disabled");
    inputs[1].removeAttribute("disabled");
    buttonSubmit.classList.remove("d-none");
    buttonEdit.classList.add("d-none");
    messageDom.classList.add("d-none");
    values.fullname = users.fullname;
    values.email = users.email;
  };

  if(message) {
    const inputs = document.querySelectorAll("input");
    const buttonSubmit = document.getElementById("submit");
    const buttonEdit = document.getElementById("edit");
    const messageDom = document.getElementById("message");
    inputs[0].setAttribute("disabled", true);
    inputs[1].setAttribute("disabled", true);
    buttonSubmit.classList.add("d-none");
    buttonEdit.classList.remove("d-none");
    messageDom.classList.remove("d-none");
    setMessage(false);
  };

  useEffect(() => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL || "https://games-website-titikkoma-be-dev.herokuapp.com") +
          `/user/${cookies.userId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        setUsers(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleForm = () => {
    if(!values.email) {
      values.email = users.email
    };
    if(!values.fullname) {
      values.fullname = users.fullname
    };
  };

  const handleSubmit = (e) => {
    handleForm();
    e.preventDefault();
    axios
      .put(
        (process.env.REACT_APP_BE_URL || "https://games-website-titikkoma-be-dev.herokuapp.com") +
          `/user/${cookies.userId}`,
        values,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => setMessage(true))
      .catch((err) => {
        alert("Invalid data, check your form");
      });
  };

  return (
    <>
        <div className={style.main}>
            <div className={style.column +" "+ "row"}>
                <Dashboard_column_left></Dashboard_column_left>
                <div className={style.column_mid +" "+ "col-md-7"}>
                  <WelcomeMessage></WelcomeMessage>
                  <div className="row mt-5 profile_section_2">
                    <div className="col-md-6">
                      <div className={profileStyle.profile_title}>
                        <i class="bi bi-caret-right-fill"></i>
                        <span>Profile</span>
                      </div>
                      <div id="message" className={profileStyle.message +" "+ "d-none"}>
                        <div class="alert alert-success" role="alert">
                          Success.
                        </div>
                      </div>
                      <div className={profileStyle.profile_box}>
                        {/* <form> */}
                        <div className={profileStyle.profile_fullname +" "+ "mb-3"}>
                          <label for="fullname" class="form-label">Full Name</label>
                          <input type="text" name="fullname" class="form-control" id="fullname" placeholder={ users.fullname } value={values.fullname == undefined ? users.fullname : values.fullname} onChange={handleChange} required disabled />
                        </div>
                        <div className={profileStyle.profile_email +" "+ "mb-3"}>
                          <label for="email" class="form-label">Your Email</label>
                          <input type="email" name="email" class="form-control" id="email" placeholder={ users.email } value={values.email == undefined ? users.email : values.email} onChange={handleChange} required disabled />
                        </div>
                        <div className="my-4">
                          <button id="submit" class="btn btn-sm btn-danger d-none" onClick={handleSubmit}>Submit Now</button>
                        </div>
                        {/* </form> */}
                        <div className="my-4">
                          <button type="submit" id="edit" class="btn btn-sm btn-danger" onClick={enableForm}>Enable Edit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <Dashboard_column_right></Dashboard_column_right>
            </div>
        </div>
    </>
  )
};

export default Index;

export async function getServerSideProps({ req, res }) {
  const cookies = getCookie('accessToken', { req, res })

  if(!cookies) {
    return {
      redirect: {
        destination: '/login'
      }
    }
  }
  
  return {
    props: {}
  }
}


