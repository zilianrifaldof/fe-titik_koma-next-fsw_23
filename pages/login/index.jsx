import { React, useEffect, useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useRouter } from 'next/router';
import { setLogin } from "../../share/redux/authSlice";
import Link from 'next/link';

function Login() {

    const [values, setValues] = useState({email: "", password: ""});
    const [warning, setWarning] = useState(false); 
    const cookies = getCookies();
    const dispatch = useDispatch();
    const router = useRouter();

    // user redux
    const auth = useSelector((state) => state.auth);

    useEffect(() => {
        console.log(`redux auth => ${JSON.stringify(auth)}`);
    }, [auth]);

    useEffect(() => {
        if(values.email.length > 5) {
            setWarning(true);
        } else {
            setWarning(false);
        }
    }, [values]); 

    const handleOnChange = (e) => {
      setValues({ ...values, [e.target.name]: e.target.value });      
    };  

    const handleSubmit = (e) => {       
      e.preventDefault();
      axios
        .post("https://games-website-titikkoma-be-dev.herokuapp.com/auth/login", values)
        .then((res) => {
          const { accessToken, id } = res.data;
          setCookie("accessToken", accessToken, { maxAge: 3600 });
          setCookie("userId", id, { maxAge: 3600 });
          // set redux to logged in
          dispatch(setLogin(accessToken));          
          router.replace("/dashboard");        
        })
        .catch((err) => {
            const data = err.response;           
            if (data.status === 400 || data.status === 404) {
                alert(data.data.message);         
            } else {           
                alert('Login Failed');      
            }           
        });
    };
   
    return (
        <div>
            <section className="vh-100" style={{backgroundColor: "#eee"}}>
                <div className="container h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-lg-4 col-xl-4">                  
                            <div className="card text-black" style={{ borderRadius: "25px" }}>
                                <div className="card-body p-md-5">                                   
                                    <div className="row justify-content-center">
                                        <div className="col-md-12 col-lg-12 col-xl-12 order-2 order-lg-1">
                                            <p className="text-center h4 fw-bold mb-1 mx-1 mx-md-4 mt-1">Login</p>
                                            <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">Hi, Enter Your Detail To Login To Your Account</p>
                                            <form className="mx-1 mx-md-4" onSubmit={handleSubmit}>                                       
                                                <div className="d-flex flex-row align-items-center mb-4">                                   
                                                    <div className="form-outline flex-fill mb-0">
                                                        <input type="email" id="email" name="email"  onChange={handleOnChange}
                                                        className="form-control" placeholder="Your Email as Username"/>
                                                         {/* { errors.email && <p className="small text-danger">{errors.email}</p>}  */}
                                                        {/* <input name="email" type="text" {...register('email')} 
                                                            className={`form-control ${errors.email ? 'is-invalid' : ''}`}/>
                                                        <div className="invalid-feedback">{errors.email?.message}</div> */}
                                                    </div>
                                                </div>
                                                <div className="d-flex flex-row align-items-center mb-4">                                  
                                                    <div className="form-outline flex-fill mb-0">
                                                        <input type="password" id="password" name="password" onChange={handleOnChange}
                                                        className="form-control" placeholder="Password" />
                                                        {/* {errors.password && <p className="small text-danger">{errors.password}</p>}    */}
                                                        {/* <input name="password" type="password" {...register('password')}
                                                        className={`form-control ${errors.password ? 'is-invalid' : ''}`} />
                                                        <div className="invalid-feedback">{errors.password?.message}</div> */}
                                                    </div>
                                                </div>                                                
                                                <p className="text-left fw-light mb-3 mt-2" style={{ color: "white", textDecoration: "none" }}>
                                                    <Link href="/forgot">                                                        
                                                        <a style={{ color: "black", textDecoration: "none" }}>Forgot Password ?</a>   
                                                    </Link>
                                                </p>
                                                <div className="d-flex flex-row align-items-center mb-4">
                                                    <button className="btn btn-md flex-fill" type="submit" 
                                                    style={{backgroundColor: "#fd7e14", borderColor: "#fd7e14"}}>Login
                                                    </button>                                                    
                                                </div>                                                 
                                            </form>
                                            <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">Don't have an account ?&nbsp;
                                                <Link href="/registration">                                                        
                                                    <a style={{ color: "black", textDecoration: "none" }}><b>Register Now</b></a>   
                                                </Link>                                           
                                            </p>
                                        </div>                           
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );    
}

export default Login;

