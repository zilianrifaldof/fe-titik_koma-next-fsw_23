import axios from "axios";
import { React, useState } from "react";
import {useRouter} from 'next/router';
import Link from 'next/link';
import {omit} from 'lodash'

function Registration() {

    const [values, setValues] = useState({});
    const [errors, setErrors] = useState({});    
    const router = useRouter();  
   
    const validate1 = () => {
        return errors.fullname || errors.email || errors.password || Object.keys(values).length < 3 ;
    };

    const validate = (event, name, value) => {  
        switch (name) {
            case 'fullname':
                if(value.length <= 2){
                    // we will set the error state
                    setErrors({
                        ...errors,
                        fullname:'Fullname atleast have 3 letters'
                    })
                } else {
                    // set the error state empty or remove the error for username input
                    //omit function removes/omits the value from given object and returns a new object
                    let newObj = omit(errors, "fullname");
                    setErrors(newObj);                    
                }
                break;        
            case 'email':
                if(
                    !new RegExp( /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value)
                ){
                    setErrors({
                        ...errors,
                        email:'Input a valid email address'
                    })
                } else {
                    let newObj = omit(errors, "email");
                    setErrors(newObj);                    
                }
            break;
            
            case 'password':
                if(
                    !new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^&*]).{8,}$/).test(value)
                ){
                    setErrors({
                        ...errors,
                        password:'Password should contains at least 8 charaters and containing uppercase, lowercase, numbers and 1 special characters'
                    })
                } else {
                    let newObj = omit(errors, "password");
                    setErrors(newObj);                    
                }
            break;            
            default:
            break;
        }
    }

    const handleChange1 = (event) => {
        //To stop default events    
        event.persist();
        let name = event.target.name;
        let val = event.target.value;        
        validate(event,name,val);
        //Let's set these values in state
        setValues({ ...values, [name]:val,});
        console.log(Object.keys(values).length);         
    }  
    
    const handleSubmit1 = (event) => {
        if(event) event.preventDefault();
        if(Object.keys(errors).length === 0 && Object.keys(values).length !== 0 ){
            // callback();
            axios
            .post("https://games-website-titikkoma-be-dev.herokuapp.com/user/registration", values)
            // .post("http://localhost:5000/user/registration", values)      
              .then((res) => {  
                  const data = res.data;
                  // console.log(res);        
                  if (res.status === 200) {
                      alert(data.message);
                      router.push("/login"); 
                  }               
              })
              .catch((err) => {
                  const data = err.response;            
                  if (data.status === 400) {
                      alert(data.data.message);         
                  } else {                
                      alert("Please, input fullname, email or password correctly!");                         
                  }
              });

        }else{
            alert("There is an Error!");
        }
    }

    const handleClick = event => {
        event.currentTarget.disabled = true;
        console.log('button clicked');
      };

    const handleChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };  
  
    const handleSubmit = (e) => {
      e.preventDefault();
      axios
      .post("https://games-website-titikkoma-be-dev.herokuapp.com/user/registration", values1)
      // .post("http://localhost:5000/user/registration", values)      
        .then((res) => {  
            const data = res.data;
            // console.log(res);        
            if (res.status === 200) {
                alert(data.message);
                router.push("/login"); 
            }               
        })
        .catch((err) => {
            const data = err.response;            
            if (data.status === 400) {
                alert(data.data.message);         
            } else {                
                alert("Please, input fullname, email or password correctly!");                         
            }
        });
    };  

    return <div>
        <section className="vh-100" style={{backgroundColor: "#eee"}}>
            <div className="container h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                <div className="col-lg-4 col-xl-4">                  
                    <div className="card text-black" style={{ borderRadius: "25px" }}>
                        <div className="card-body p-md-5">
                            <div className="row justify-content-center">
                                <div className="col-md-12 col-lg-12 col-xl-12 order-2 order-lg-1">
                                    <p className="text-center h4 fw-bold mb-1 mx-1 mx-md-4 mt-1">Create Account</p>
                                    <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">Share Your Highest Score To The World From Today</p>
                                    <form className="mx-1 mx-md-4" onSubmit={handleSubmit1}>
                                        <div className="d-flex flex-row align-items-center mb-4">                                    
                                            <div className="form-outline flex-fill mb-0">                                       
                                                <input type="tex" id="fullname" name="fullname" onChange={handleChange1} minLength="3"
                                                    className="form-control" placeholder="Your Fullname"/>
                                                     {errors.fullname && <p className="small text-danger">{errors.fullname}</p>}                          
                                            </div>
                                        </div>
                                        <div className="d-flex flex-row align-items-center mb-4">                                   
                                            <div className="form-outline flex-fill mb-0">
                                            <input type="email" id="email" name="email"  onChange={handleChange1} minLength="8"                                          
                                                className="form-control" placeholder="Your Email as Username"/> 
                                                { errors.email && <p className="small text-danger">{errors.email}</p>}                                
                                            </div>
                                        </div>
                                        <div className="d-flex flex-row align-items-center mb-4">                                  
                                            <div className="form-outline flex-fill mb-0"> 
                                                <input type="password" id="password" name="password"  onChange={handleChange1}                                                 
                                                className="form-control" placeholder="Password" />  
                                                {errors.password && <p className="small text-danger">{errors.password}</p>}                                                                      
                                            </div>
                                        </div>
                                        <div className="d-flex flex-row align-items-center mb-4">
                                            <button className="btn btn-md flex-fill" type="submit" disabled={validate1()}
                                                style={{backgroundColor: "#fd7e14", borderColor: "#fd7e14"}}>Create Now
                                            </button>
                                        </div>                               
                                        {/* <div className="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                            <button className="btn btn-primary btn-sm" type="submit">Create Now</button>
                                        </div> */}
                                    </form>                                   
                                    <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">Already have an account ?&nbsp;
                                        <Link href="/login">                                                        
                                            <a style={{ color: "black", textDecoration: "none" }}><b>Login Now</b></a>   
                                        </Link>                                           
                                    </p>
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>
}

export default Registration;