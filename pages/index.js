import styles from "../styles/Home.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { motion } from "framer-motion";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import ParticlesBackground from "../components/tsparticle/background";

export default function Home() {
  const [leaders, setLeaders] = useState([]);
  const auth = useSelector((state) => state.auth);
  let play;
  if (!auth.isLoggedIn) {
    play = "/login";
  } else {
    play = "/dashboard";
  }
  const fetchLeaders = () => {
    axios
      .get(
        (process.env.REACT_APP_BE_URL ||
          "https://games-website-titikkoma-be-dev.herokuapp.com") +
          "/users?pageNumber=1&limitParm=3"
      )
      .then((res) => {
        const listLeaders = res.data;
        setLeaders(listLeaders);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    fetchLeaders();
  }, []);

  const fadeLeft = {
    hidden: { opacity: 0, x: 2000 },
    visible: { opacity: 1, x: 0 },
  };
  return (
    <>
    <ParticlesBackground></ParticlesBackground>
      <Container className={styles.main} id="particles-js">
        <Row className="justify-content-md-center">
          <Col lg={6}>
            <motion.img
              variants={fadeLeft}
              initial="hidden"
              animate="visible"
              transition={{ duration: 1 }}
              src="landingPage.jpg"
              alt="Error"
            />
          </Col>
          <Col lg={6}>
            <motion.center
              variants={fadeLeft}
              initial="hidden"
              animate="visible"
              transition={{ duration: 4 }}
            >
              <h1>You Want To Play?!</h1>
              <Link href={play}>
                <motion.button
                  whileHover={{ scale: 1.05 }}
                  whileTap={{
                    scale: 0.95,
                    backgroundColor: "lightblue",
                    border: "none",
                  }}
                >
                  {" "}
                  Let's Play !{" "}
                </motion.button>
              </Link>
            </motion.center>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <motion.center
              variants={fadeLeft}
              initial="hidden"
              animate="visible"
              transition={{ duration: 2 }}
            >
              <h2>About</h2>
              <p>
                {" "}
                Play simple web games such as Rock-Paper-Scissors, Hangman, and
                Mistery Number. Win and get your name to our Leader Board!
                {/* Rock-Paper-Scissors is a two-person hand game. This game is
                often used for random selection, as well as tossing coins, dice,
                and more. Among Indonesian children, this game is also known as
                "Japanese Suwit". */}
              </p>
            </motion.center>
          </Col>
          <Col lg={4}>
            <motion.div
              variants={fadeLeft}
              initial="hidden"
              animate="visible"
              transition={{ duration: 3 }}
            >
              <center>
                <h2>Features</h2>
              </center>
              <ul>
                <li>Play Versus Computer</li>
                <li>Play Versus Other Player</li>
              </ul>
            </motion.div>
          </Col>
          <Col lg={4}>
            <motion.div
              variants={fadeLeft}
              initial="hidden"
              animate="visible"
              transition={{ duration: 4 }}
            >
              <center>
                <h2>Leader Board</h2>
              </center>
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Score</th>
                  </tr>
                </thead>
                <tbody className="text-capitalize">
                  {leaders.map((leader, index) => {
                    return (
                      <tr>
                        <th scope="row">{index + 1}</th>
                        <td>{leader.fullname}</td>
                        <td>{leader.score}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </motion.div>
          </Col>
        </Row>
      </Container>
    </>
  );
}
