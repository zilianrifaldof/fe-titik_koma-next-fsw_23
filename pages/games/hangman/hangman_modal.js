import { useState } from "react";

const HangmanModal = (props) => {
  const [show, setShow] = useState(true);

  const handleClose = () => {
    setShow(!show);
    props.onClose();
  };

  if (!show) {
    return (
      <>
        <button className="btn btn-outline-primary" onClick={handleClose}>
          How to Play
        </button>
      </>
    );
  } else {
    return (
      <div style={styles.modal} onClick={handleClose}>
        <div style={styles.modalContent}>
          <div style={styles.btnContainer}>
            <button onClick={handleClose} style={styles.btnCross}>
              X
            </button>
          </div>
          <div>
            <center>
              <h1 style={styles.heading}>How To Play ?</h1>
            </center>
          </div>
          <ol style={styles.list}>
            <li>The page will randomize a word for you to guess.</li>
            <li>The randomize word will then be shown as empty letter</li>
            <li>Guess your letter by clicking the alphabet button.</li>
            <li>
              If the letter you choose is correct then it will fill the empty
              word.
            </li>
            <li>
              You have 6 chance to guess the word, if you lose it all you will
              lose.
            </li>
          </ol>
        </div>
      </div>
    );
  }
};

const styles = {
  heading: {
    marginTop: "5px",
  },
  btnContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
  btnCross: {
    border: "none",
    background: "none",
    cursor: "pointer",
  },
  modal: {
    width: "100%",
    left: "0",
    rigth: "0",
    top: "0",
    bottom: "0",
    position: "fixed",
    display: "flex",
    backgroundColor: "rgba(0,0,0,0.5)",
    alignItems: "center",
    justifyContent: "center",
  },
  modalContent: {
    borderRadius: "10%",
    aspectRatio: "1/1",
    padding: "25px",
    width: "450px",
    backgroundColor: "rgba(225,225,225,0.9)",
  },
  list: {
    lineHeight: "1.5em",
  },
};

export default HangmanModal;
