import { useEffect, useState } from "react";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import axios from "axios";
import "../../../styles/hangman.module.css";
import Picture0 from "../../../public/img/hangman/00.jpg";
import Picture1 from "../../../public/img/hangman/01.jpg";
import Picture2 from "../../../public/img/hangman/02.jpg";
import Picture3 from "../../../public/img/hangman/03.jpg";
import Picture4 from "../../../public/img/hangman/04.jpg";
import Picture5 from "../../../public/img/hangman/05.jpg";
import Picture6 from "../../../public/img/hangman/06.jpg";
import Image from "next/image";
import HangmanModal from "./hangman_modal";
import Dashboard_column_left from "../../../components/Dashboard_column_left";
import Dashboard_column_right from "../../../components/Dashboard_column_right";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
} from "next-share";

const Hangman = () => {
  const images = [
    Picture0,
    Picture1,
    Picture2,
    Picture3,
    Picture4,
    Picture5,
    Picture6,
  ];
  const cookies = getCookies();
  const alphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];
  const wordList = [
    "time",
    "jump",
    "trap",
    "monkey",
    "horse",
    "duck",
    "juice",
    "america",
    "pickle",
    "continue",
    "peanuts",
    "lady",
    "sydney",
    "pumpkin",
    "computer",
    "phyton",
    "ruby",
    "java",
    "javascript",
  ];
  const answer = wordList[Math.floor(Math.random() * wordList.length)];
  const [life, setLife] = useState(6);
  const [word, setWord] = useState([]);
  const [form, setForm] = useState([]);
  const [win, setWin] = useState(false);
  const [once, setOnce] = useState(false);
  const [picture, setPicture] = useState(0);
  const [playState, setPlayState] = useState(true);
  const [close, setClose] = useState(false);

  const handleClose = () => {
    setClose(!close);
  };

  const checkAnswer = (alpha) => {
    const form1 = [...form];
    let isIn = false;
    for (let i = 0; i < form.length; i++) {
      if (word[i] == alpha) {
        form1[i] = alpha;
        isIn = true;
      }
    }
    if (!isIn) {
      setLife(life - 1);
      setPicture(picture + 1);
    }
    setForm(form1);
  };

  const checkWin = () => {
    if (word == form.join("")) {
      setWin(true);
      setPlayState(false);
      axios
        .post(
          //(process.env.REACT_APP_BE_URL || "http://localhost:5000") +
          "https://games-website-titikkoma-be-dev.herokuapp.com" +
            `/user/addScore/${cookies.userId}`,
          {},
          { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
        )
        .then((res) => console.log("Game Ended"))
        .catch((error) => console.log(error));
    }
  };

  const handleClick = (e) => {
    checkAnswer(e.target.value);
    e.target.classList.add("invisible-hangman");
    e.target.disabled = true;
    if (life <= 1) {
      setPlayState(false);
    }
  };

  const handleReset = () => {
    window.location.reload();
  };

  useEffect(() => {
    if (once != true) {
      const blank = [];
      for (let i = 0; i < answer.length; i++) {
        blank.push("_");
      }
      setForm(blank);
      setWord(answer);
      setOnce(true);
    } else {
      checkWin();
    }
  }, [form, playState]);

  return playState ? (
    <>
      <div className="row" style={{ width: "100%" }}>
        <Dashboard_column_left />
        <div className="col-md-7" style={{ padding: "0px 30px" }}>
          <center>
            <h1 style={{ marginTop: "4vh" }}>Hangman Game</h1>
            <div>
              {close ? (
                <Image height="350px" width="300px" src={images[picture]} />
              ) : (
                <></>
              )}
            </div>
            <div style={{ letterSpacing: "7px", fontSize: "2em" }}>{form}</div>
            <div>
              <div style={{ marginTop: "25px" }}>
                <div
                  style={{
                    width: "300px",
                    aspectRatio: "7/4",
                    display: "grid",
                    gridTemplateColumns: "auto auto auto auto auto auto auto",
                  }}
                >
                  {alphabet.map((alph) => {
                    return (
                      <>
                        <button
                          key={alph}
                          style={{
                            fontSize: "1em",
                            padding: "2px",
                            margin: "3px",
                            boxShadow: "3px 3px #00000055",
                            borderRadius: "10px",
                          }}
                          value={`${alph}`}
                          onClick={handleClick}
                        >
                          {alph.toUpperCase()}
                        </button>
                      </>
                    );
                  })}
                </div>
              </div>
              <h2>Your Life {life}</h2>
            </div>
            <div>
              <HangmanModal onClose={handleClose} close={close} />
            </div>
          </center>
        </div>
        <Dashboard_column_right />
      </div>
    </>
  ) : (
    <>
      <div className="row" style={{ width: "100%" }}>
        <Dashboard_column_left />

        <div className="col-md-7" style={{ padding: "0px 30px" }}>
          <center>
            <div>
              <h1 style={{ marginTop: "4vh" }}>Hangman Game</h1>
            </div>
            <div>
              <Image height="350px" width="300px" src={images[picture]} />
            </div>
            <div
              style={{
                marginTop: "5vh",
                fontWeight: "bold",
                fontSize: "1.75em",
              }}
            >
              The correct word is {word}
            </div>
            <h1>The game has ended, you {win ? "win" : "lose"}. </h1>
            <div style={{ marginTop: "10px" }}>Share It :</div>
            <div style={{ marginTop: "10px" }}>
              <FacebookShareButton
                url={"https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"}
                quote={"Let's play together in Titik Koma Website."}
                hashtag={"#titikoma"}
              >
                <FacebookIcon size={32} round />
              </FacebookShareButton>
              <span style={{ margin: "8px" }}></span>
              <TwitterShareButton
                url={"https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"}
                title={"Let's play together in Titik Koma Website."}
              >
                <TwitterIcon size={32} round />
              </TwitterShareButton>
            </div>
            <button
              style={{
                width: "200px",
                fontSize: "18px",
                backgroundColor: "purple",
                padding: "10px",
                borderRadius: "25px",
                color: "white",
              }}
              onClick={handleReset}
            >
              New Game
            </button>
          </center>
        </div>
        <Dashboard_column_right />
      </div>
    </>
  );
};

export default Hangman;

export async function getServerSideProps({ req, res }) {
  const cookies = getCookie('accessToken', { req, res })

  if(!cookies) {
    return {
      redirect: {
        destination: '/login'
      }
    }
  }
  
  return {
    props: {}
  }
}
