import Image from "next/image";
import { useState } from "react";
import RpsModal from "./rpsmodal";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import rock from "../../../public/img/rps/rock.png";
import paper from "../../../public/img/rps/paper.png";
import scissor from "../../../public/img/rps/scissor.png";
import axios from "axios";
import Dashboard_column_left from "../../../components/Dashboard_column_left";
import Dashboard_column_right from "../../../components/Dashboard_column_right";
import WelcomeMessage from "../../../components/WelcomeMessage";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
} from "next-share";
import style from "../../../styles/Dashboard.module.css";

const Rps = () => {
  const choiceArr = ["R", "P", "S"];
  const cookies = getCookies();
  const [userChoice, setUserChoice] = useState(null);
  const [win, setWin] = useState(null);
  const [comChoice, setComChoice] = useState(null);
  const [close, setClose] = useState(false);
  const [message, setMessage] = useState(null);

  const handleReset = () => {
    window.location.reload();
  };

  const handleClose = () => {
    setClose(!close);
  };

  const handleClick = (e) => {
    e.preventDefault();
    setMessage(null);
    if (userChoice === null) {
      setUserChoice(e.currentTarget.value);
      setComChoice(choiceArr[Math.floor(Math.random() * 3)]);
    } else {
      setUserChoice(e.currentTarget.value);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (userChoice) {
      if (
        (userChoice === "R" && comChoice === "S") ||
        (userChoice === "P" && comChoice === "R") ||
        (userChoice === "S" && comChoice === "P")
      ) {
        setWin("Player");
        axios
          .post(
            //(process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            "https://games-website-titikkoma-be-dev.herokuapp.com" +
              `/user/addScore/${cookies.userId}`,
            {},
            { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
          )
          .then((res) => console.log("Game Ended"))
          .catch((error) => console.log(error));
      } else if (userChoice === comChoice) {
        setWin("DRAW");
      } else {
        setWin("Computer");
      }
    } else {
      setMessage("Please choose a valid option.");
    }
  };

  return !win ? (
    <>
      <div className={style.main + " " + "fluid-container"}>
        <div className={style.column + " " + "row"}>
        <Dashboard_column_left />
        <div className={style.container + " " + "col-md-7"}>
          <div className={style.column_mid}>
          <WelcomeMessage></WelcomeMessage>
            <center>
            <h1 style={{ marginTop: "40px" }}>Rock paper scissor</h1>
            <form onSubmit={handleSubmit}>
              <div style={{ marginTop: "8vh" }}>
                {close ? (
                  <>
                    <button
                      key="R"
                      value="R"
                      onClick={handleClick}
                      style={{ border: "none", background: "none" }}
                    >
                      <Image src={rock} title="Rock" width="100px" height="100px"></Image>
                    </button>
                    <button
                      key="P"
                      value="P"
                      onClick={handleClick}
                      style={{
                        border: "none",
                        background: "none",
                      }}
                    >
                      <Image src={paper} title="Paper" width="100px" height="100px" />
                    </button>
                    <button
                      key="S"
                      value="S"
                      onClick={handleClick}
                      style={{ border: "none", background: "none" }}
                    >
                      <Image src={scissor} title="Scissor" width="100px" height="100px" />
                    </button>
                  </>
                ) : (
                  <></>
                )}
              </div>
              {userChoice ? (
                <div style={{ marginTop: "2vh" }}>
                  You pick :{" "}
                  <div style={{ marginTop: "1vh" }}>
                    {userChoice === "R" ? (
                      <>
                        <br />
                        <Image src={rock} width="100px" height="100px"></Image>
                      </>
                    ) : userChoice === "P" ? (
                      <>
                        <br />
                        <Image src={paper} width="100px" height="100px"></Image>
                      </>
                    ) : userChoice === "S" ? (
                      <>
                        <br />
                        <Image
                          src={scissor}
                          width="100px"
                          height="100px"
                        ></Image>
                      </>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
              ) : (
                <></>
              )}
              {<div className="text-danger">{message}</div>}
              <button
                style={{
                  marginTop: "3vh",
                  width: "200px",
                  fontSize: "18px",
                  backgroundColor: "orange",
                  padding: "10px",
                  borderRadius: "25px",
                  color: "white",
                }}
                type="submit"
              >
                Submit Choice
              </button>
            </form>
            <div style={{ marginTop: "3vh" }}>
              <RpsModal onClose={handleClose} />
            </div>
            </center>
          </div>
        </div>
        <Dashboard_column_right />
        </div>
      </div>
    </>
  ) : (
    <>
      <div className={style.main + " " + "fluid-container"}>
        <div className={style.column + " " + "row"}>
          <Dashboard_column_left />
          <div className="col-md-7" style={{ padding: "0px 30px" }}>
            <center>
              <h1 style={{ marginTop: "4vh" }}>Rock paper scissor</h1>
              <div
                style={{ display: "flex", width: "400px", paddingTop: "10vh" }}
              >
                <h3>You Pick</h3>
                {userChoice === "R" ? (
                  <>
                    <br />
                    <Image src={rock} width="100px" height="100px"></Image>
                  </>
                ) : userChoice === "P" ? (
                  <>
                    <br />
                    <Image src={paper} width="100px" height="100px"></Image>
                  </>
                ) : userChoice === "S" ? (
                  <>
                    <br />
                    <Image src={scissor} width="100px" height="100px"></Image>
                  </>
                ) : (
                  <></>
                )}
                <h3>Com Pick</h3>
                {comChoice === "R" ? (
                  <>
                    <br />
                    <Image src={rock} width="100px" height="100px"></Image>
                  </>
                ) : comChoice === "P" ? (
                  <>
                    <br />
                    <Image src={paper} width="100px" height="100px"></Image>
                  </>
                ) : comChoice === "S" ? (
                  <>
                    <br />
                    <Image src={scissor} width="100px" height="100px"></Image>
                  </>
                ) : (
                  <></>
                )}
              </div>
              <div style={{ paddingTop: "3vh" }}>
                {win !== "DRAW" ? (
                  <>The Winner is {win}</>
                ) : (
                  <>The games end with DRAW</>
                )}
              </div>
              <div style={{ marginTop: "10px" }}>Share It :</div>
              <div style={{ marginTop: "10px" }}>
                <FacebookShareButton
                  url={"https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"}
                  quote={"Let's play together in Titik Koma Website."}
                  hashtag={"#titikoma"}
                >
                  <FacebookIcon size={32} round />
                </FacebookShareButton>
                <span style={{ margin: "8px" }}></span>
                <TwitterShareButton
                  url={"https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"}
                  title={"Let's play together in Titik Koma Website."}
                >
                  <TwitterIcon size={32} round />
                </TwitterShareButton>
              </div>
              <button
                style={{
                  marginTop: "3vh",
                  width: "200px",
                  fontSize: "18px",
                  backgroundColor: "purple",
                  padding: "10px",
                  borderRadius: "25px",
                  color: "white",
                }}
                onClick={handleReset}
              >
                Reset Game
              </button>
            </center>
          </div>
          <Dashboard_column_right />
        </div>
      </div>
    </>
  );
};

export default Rps;

export async function getServerSideProps({ req, res }) {
  const cookies = getCookie('accessToken', { req, res })

  if(!cookies) {
    return {
      redirect: {
        destination: '/login'
      }
    }
  }
  
  return {
    props: {}
  }
}
