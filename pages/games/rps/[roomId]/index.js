import React, { useState, useEffect } from "react";
import Image from "next/image";
import axios from "axios";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useRouter } from "next/router";
import RpsModal from "../rpsmodal";
import rock from "../../../../public/img/rps/rock.png";
import paper from "../../../../public/img/rps/paper.png";
import scissor from "../../../../public/img/rps/scissor.png";
import Dashboard_column_left from "../../../../components/Dashboard_column_left";
import Dashboard_column_right from "../../../../components/Dashboard_column_right";
import WelcomeMessage from "../../../../components/WelcomeMessage";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
} from "next-share";
import style from "../../../../styles/Dashboard.module.css";
import loader from "../../../../styles/Loader.module.css"

const Game = () => {
  const router = useRouter();
  const cookies = getCookies();
  const [userChoice, setUserChoice] = useState(null);
  const [player, setPlayer] = useState("");
  const [gameStatus, setGameStatus] = useState(null);
  const [close, setClose] = useState(false);
  const [message, setMessage] = useState(null);
  const [sameId, setSameId] = useState(false);
  const { roomId } = router.query;

  useEffect(() => {
    if (router.isReady) {
      if (!roomId) {
        alert("The room does not exist.");
        router.push("/dashboard");
      }
      __fetchGameStatus();
    }
    if (gameStatus) {
      if (gameStatus.userId1 == cookies.userId && gameStatus.winner === null) {
        setSameId(true);
      }
    }
  }, [router.isReady, gameStatus]);

  const __fetchGameStatus = () => {
    axios
      .get(
        //(process.env.REACT_APP_BE_URL || "http://localhost:5000") +
        "https://games-website-titikkoma-be-dev.herokuapp.com" +
          `/game/${roomId}`,
        {
          headers: { Authorization: `Bearer ${cookies.accessToken}` },
        }
      )
      .then((res) => {
        const gamePlay = res.data;
        if (gamePlay.userId1 === null) {
          setPlayer("player1");
        } else {
          setPlayer("player2");
        }
        setGameStatus(gamePlay);
      })
      .catch((error) => console.log(error));
  };

  const handleClose = () => {
    setClose(!close);
  };

  const handleClick = (e) => {
    e.preventDefault();
    setMessage(null);
    setUserChoice(e.currentTarget.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (userChoice) {
      if (gameStatus.userId1 == cookies.userId) {
        alert(
          "You have choosen an option. Please wait till the other users pick their option"
        );
        router.push("/dashboard");
      } else {
        axios
          .put(
            //(process.env.REACT_APP_BE_URL || "http://localhost:5000") +
            "https://games-website-titikkoma-be-dev.herokuapp.com" +
              `/game/play/${gameStatus.id}`,
            { player: player, userId: cookies.userId, userChoice: userChoice },
            { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
          )
          .then((res) => {
            window.location.reload();
          })
          .catch((error) => console.log(error));
      }
    } else {
      setMessage("Please choose a valid option.");
    }
  };

  return gameStatus ? (
    <>
      <div className={style.main + " " + "fluid-container"}>
        <div className={style.column + " " + "row"}>
          <Dashboard_column_left />
          <div className="col-md-7" style={{ padding: "0px 30px" }}>
            <div className={style.column_mid}>
              <center>
                <h1 style={{ marginTop: "4vh" }}>Multiplayer Rock Paper Scissor</h1>
                <div>
                  <div>
                    <h3 style={{ marginTop: "3vh" }}>Room : {gameStatus.name}</h3>
                  </div>
                  <div>
                    <h4 style={{ marginTop: "3vh" }}>{gameStatus.description}</h4>
                  </div>
                  {sameId ? (
                    <>
                      <h3>
                        Your have already choosen an option please wait another user
                        to play.
                      </h3>
                      <div>
                        You pick :{" "}
                        {gameStatus.user1_choice === "R" ? (
                          <>
                            <br />
                            <Image src={rock} width="100px" height="100px"></Image>
                          </>
                        ) : gameStatus.user1_choice === "P" ? (
                          <>
                            <br />
                            <Image src={paper} width="100px" height="100px"></Image>
                          </>
                        ) : gameStatus.user1_choice === "S" ? (
                          <>
                            <br />
                            <Image
                              src={scissor}
                              width="100px"
                              height="100px"
                            ></Image>
                          </>
                        ) : (
                          <></>
                        )}
                      </div>
                    </>
                  ) : gameStatus.winner === null ? (
                    <>
                      <div>
                        <p style={{ marginTop: "3vh" }}>Choose your option</p>
                      </div>
                      <form onSubmit={handleSubmit}>
                        <div style={{ marginTop: "8vh" }}>
                          {close ? (
                            <>
                              <button
                                key="R"
                                value="R"
                                onClick={handleClick}
                                style={{ border: "none", background: "none" }}
                              >
                                <Image
                                  src={rock}
                                  width="100px"
                                  height="100px"
                                ></Image>
                              </button>
                              <button
                                key="P"
                                value="P"
                                onClick={handleClick}
                                style={{ border: "none", background: "none" }}
                              >
                                <Image src={paper} width="100px" height="100px" />
                              </button>
                              <button
                                key="S"
                                value="S"
                                onClick={handleClick}
                                style={{ border: "none", background: "none" }}
                              >
                                <Image src={scissor} width="100px" height="100px" />
                              </button>
                            </>
                          ) : (
                            <></>
                          )}
                        </div>
                        {userChoice ? (
                          <div>
                            You pick :{" "}
                            {userChoice === "R" ? (
                              <>
                                <br />
                                <Image
                                  src={rock}
                                  width="100px"
                                  height="100px"
                                ></Image>
                              </>
                            ) : userChoice === "P" ? (
                              <>
                                <br />
                                <Image
                                  src={paper}
                                  width="100px"
                                  height="100px"
                                ></Image>
                              </>
                            ) : userChoice === "S" ? (
                              <>
                                <br />
                                <Image
                                  src={scissor}
                                  width="100px"
                                  height="100px"
                                ></Image>
                              </>
                            ) : (
                              <></>
                            )}
                          </div>
                        ) : (
                          <></>
                        )}
                        {<div className="text-danger">{message}</div>}
                        <button
                          style={{
                            marginTop: "3vh",
                            width: "200px",
                            fontSize: "18px",
                            backgroundColor: "orange",
                            padding: "10px",
                            borderRadius: "25px",
                            color: "white",
                          }}
                          type="submit"
                        >
                          Submit Choice
                        </button>
                      </form>
                      <div style={{ marginTop: "3vh" }}>
                        <RpsModal onClose={handleClose} />
                      </div>
                    </>
                  ) : (
                    <></>
                  )}

                  {gameStatus.winner !== null ? (
                    <>
                      <h2>This room winner is {gameStatus.winner}</h2>
                      <div
                        style={{
                          display: "flex",
                          width: "500px",
                          paddingTop: "10vh",
                        }}
                      >
                        <h3>Player 1 chooses</h3>
                        {gameStatus.user1_choice === "R" ? (
                          <>
                            <br />
                            <Image src={rock} width="100px" height="100px"></Image>
                          </>
                        ) : gameStatus.user1_choice === "P" ? (
                          <>
                            <br />
                            <Image src={paper} width="100px" height="100px"></Image>
                          </>
                        ) : gameStatus.user1_choice === "S" ? (
                          <>
                            <br />
                            <Image
                              src={scissor}
                              width="100px"
                              height="100px"
                            ></Image>
                          </>
                        ) : (
                          <></>
                        )}
                        <h3>Player 2 chooses</h3>
                        {gameStatus.user2_choice === "R" ? (
                          <>
                            <br />
                            <Image src={rock} width="100px" height="100px"></Image>
                          </>
                        ) : gameStatus.user2_choice === "P" ? (
                          <>
                            <br />
                            <Image src={paper} width="100px" height="100px"></Image>
                          </>
                        ) : gameStatus.user2_choice === "S" ? (
                          <>
                            <br />
                            <Image
                              src={scissor}
                              width="100px"
                              height="100px"
                            ></Image>
                            <div style={{ marginTop: "10px" }}>Share It :</div>
                            <div style={{ marginTop: "10px" }}>
                              <FacebookShareButton
                                url={
                                  "https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"
                                }
                                quote={"Let's play together in Titik Koma Website."}
                                hashtag={"#titikoma"}
                              >
                                <FacebookIcon size={32} round />
                              </FacebookShareButton>
                              <span style={{ margin: "8px" }}></span>
                              <TwitterShareButton
                                url={
                                  "https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"
                                }
                                title={"Let's play together in Titik Koma Website."}
                              >
                                <TwitterIcon size={32} round />
                              </TwitterShareButton>
                            </div>
                          </>
                        ) : (
                          <></>
                        )}
                      </div>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </center>
            </div>
          </div>
          <Dashboard_column_right />
        </div>
      </div>
    </>
  ) : (
    <>
      <div className={style.main}>
        <div class={loader.loader}></div>
      </div>
    </>
  );
};

export default Game;

export async function getServerSideProps({ req, res }) {
  const cookies = getCookie('accessToken', { req, res })

  if(!cookies) {
    return {
      redirect: {
        destination: '/login'
      }
    }
  }
  
  return {
    props: {}
  }
}
