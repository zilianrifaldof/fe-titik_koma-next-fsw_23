import { useState } from "react";
import rules from "../../../public/img/rps/rules.png";
import Image from "next/image";

const RpsModal = (props) => {
  const [show, setShow] = useState(true);

  const handleClick = () => {
    setShow(!show);
    props.onClose();
  };

  if (!show) {
    return (
      <button className="btn btn-outline-primary" onClick={handleClick}>
        How to Play
      </button>
    );
  } else {
    return (
      <div style={styles.modal} onClick={handleClick}>
        <div style={styles.modalContent}>
          <div style={styles.btnContainer}>
            <button onClick={handleClick} style={styles.btnCross}>
              X
            </button>
          </div>
          <div>
            <center>
              <h1 style={styles.heading}>How To Play ?</h1>
            </center>
          </div>
          <ol style={styles.list}>
            <li>Choose Your Option in From the button.</li>
            <li>Your choice will show after you picked it.</li>
            <li>
              After you are sure with your choice click the submit button.
            </li>
            <li>The winner will be shown in the next screen.</li>
          </ol>
          <center>
            <Image src={rules} />
          </center>
        </div>
      </div>
    );
  }
};

const styles = {
  heading: {
    marginTop: "5px",
  },
  btnContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
  btnCross: {
    border: "none",
    background: "none",
    cursor: "pointer",
    fontSize: '28px'
  },
  modal: {
    width: "100%",
    left: "0",
    rigth: "0",
    top: "0",
    bottom: "0",
    position: "fixed",
    display: "flex",
    backgroundColor: "rgba(0,0,0,0.5)",
    alignItems: "center",
    justifyContent: "center",
  },
  modalContent: {
    borderRadius: "10%",
    aspectRatio: "1/1",
    padding: "25px",
    width: "450px",
    backgroundColor: "rgba(225,225,225,0.9)",
  },
  list: {
    lineHeight: "1.3em",
  },
};

export default RpsModal;
