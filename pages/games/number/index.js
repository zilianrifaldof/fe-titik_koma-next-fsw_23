import { useState } from "react";
import NumberModal from "./numberguess_modal";
import axios from "axios";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import Dashboard_column_left from "../../../components/Dashboard_column_left";
import Dashboard_column_right from "../../../components/Dashboard_column_right";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
} from "next-share";

const NumberGuess = () => {
  const cookies = getCookies();
  const [userChoice, setUserChoice] = useState(null);
  const [win, setWin] = useState(null);
  const [life, setLife] = useState(6);
  const [message, setMessage] = useState(null);
  const [answer, setAnswer] = useState(null);

  const handleReset = () => {
    window.location.reload();
  };

  const handleClose = () => {
    setClose(!close);
  };

  const handleChange = (e) => {
    e.preventDefault();
    if (answer === null) {
      setAnswer(Math.floor(Math.random() * 100) + 1);
    }
    setUserChoice(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (parseInt(userChoice) > 0 && parseInt(userChoice) < 101) {
      if (answer !== null) {
        if (answer === parseInt(userChoice)) {
          setWin(true);
          axios
            .post(
              //(process.env.REACT_APP_BE_URL || "http://localhost:5000") +
              "https://games-website-titikkoma-be-dev.herokuapp.com" +
                `/user/addScore/${cookies.userId}`,
              {},
              { headers: { Authorization: `Bearer ${cookies.accessToken}` } }
            )
            .then((res) => console.log("Game Ended"))
            .catch((error) => console.log(error));
        } else {
          setLife(life - 1);
          if (answer > parseInt(userChoice)) {
            setMessage("Your choice is lower than the answer.");
          } else {
            setMessage("Your choice is higher than the answer.");
          }
        }
      }
    } else {
      alert("Please input a valid asnwer between 1 and 100");
    }
  };

  return !win && life > 0 ? (
    <>
      <div className="row" style={{ width: "100%" }}>
        <Dashboard_column_left />
        <div className="col-md-7" style={{ padding: "0px 30px" }}>
          <center>
            <h1 style={{ marginTop: "4vh" }}>Number Guessing Game</h1>
            <p>Please Input your guess between 1 100</p>
            <form onSubmit={handleSubmit}>
              <div style={{ marginTop: "5vh" }}>
                <label style={{ fontSize: "20px" }}>Your Guess</label>
                <div>
                  <input
                    onChange={handleChange}
                    style={{
                      textAlign: "center",
                      height: "75px",
                      width: "75px",
                      fontSize: "36px",
                      marginTop: "2vh",
                      borderRadius: "5px",
                      boxShadow: "3px 3px #00000044",
                    }}
                  ></input>
                </div>
              </div>
              <div style={{ marginTop: "3vh" }}>{message}</div>
              <button
                type="submit"
                style={{
                  marginTop: "4vh",
                  width: "200px",
                  fontSize: "18px",
                  backgroundColor: "orange",
                  padding: "10px",
                  borderRadius: "25px",
                  color: "white",
                }}
              >
                Submit
              </button>
            </form>
            <div style={{ marginTop: "3vh" }}>
              <NumberModal onClose={handleClose} />
            </div>
          </center>
        </div>
        <Dashboard_column_right />
      </div>
    </>
  ) : (
    <>
      <div className="row" style={{ width: "100%" }}>
        <Dashboard_column_left />
        <div className="col-md-7" style={{ padding: "0px 30px" }}>
          <center>
            <div style={{ fontFamily: "Helvetica,Arial" }}>
              <h1 style={{ marginTop: "4vh" }}>Number Guessing Game</h1>
              <div>
                <div>
                  <h2 style={{ marginTop: "10vh" }}>The Answer is {answer}</h2>
                </div>
                {win ? (
                  <>
                    <h2>Your Win!!!!</h2>
                    <div>Your guess it right in {6 - life + 1} tries</div>
                  </>
                ) : (
                  <>You lose all your life. Game Over !!!</>
                )}
              </div>
              <div style={{ marginTop: "10px" }}>Share It :</div>
              <div style={{ marginTop: "10px" }}>
                <FacebookShareButton
                  url={
                    "https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"
                  }
                  quote={"Let's play together in Titik Koma Website."}
                  hashtag={"#titikoma"}
                >
                  <FacebookIcon size={32} round />
                </FacebookShareButton>
                <span style={{ margin: "8px" }}></span>
                <TwitterShareButton
                  url={
                    "https://fe-next-fsw23-git-dev-zilianrifaldof.vercel.app"
                  }
                  title={"Let's play together in Titik Koma Website."}
                >
                  <TwitterIcon size={32} round />
                </TwitterShareButton>
              </div>
              <button
                style={{
                  marginTop: "4vh",
                  width: "200px",
                  fontSize: "18px",
                  backgroundColor: "orange",
                  padding: "10px",
                  borderRadius: "25px",
                  color: "white",
                }}
                onClick={handleReset}
              >
                Reset Game
              </button>
            </div>
          </center>
        </div>
        <Dashboard_column_right />
      </div>
    </>
  );
};

export default NumberGuess;

export async function getServerSideProps({ req, res }) {
  const cookies = getCookie('accessToken', { req, res })

  if(!cookies) {
    return {
      redirect: {
        destination: '/login'
      }
    }
  }
  
  return {
    props: {}
  }
}
