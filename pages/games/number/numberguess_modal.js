import { useState } from "react";

const NumberModal = () => {
  const [show, setShow] = useState(true);

  const handleClick = () => {
    setShow(!show);
  };

  if (!show) {
    return (
      <>
        <button className="btn btn-outline-primary" onClick={handleClick}>
          How to Play
        </button>
      </>
    );
  } else {
    return (
      <div style={styles.modal} onClcik={handleClick}>
        <div style={styles.modalContent}>
          <div style={styles.btnContainer}>
            <button onClick={handleClick} style={styles.btnCross}>
              X
            </button>
          </div>
          <div>
            <center>
              <h1 style={styles.heading}>How To Play ?</h1>
            </center>
          </div>
          <ol style={styles.list}>
            <li>Input your answer in the text box.</li>
            <li>The number must be between 1 ~ 100.</li>
            <li>
              After you are sure with your choice submit it with the submit
              button.
            </li>
            <li>Your have 6 chance to guess the answer.</li>
            <li>
              The game is over if you lose all your chances or you guess it
              right.
            </li>
          </ol>
        </div>
      </div>
    );
  }
};

const styles = {
  heading: {
    marginTop: "5px",
  },
  btnContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
  btnCross: {
    border: "none",
    background: "none",
    cursor: "pointer",
    fontSize: '28px'
  },
  modal: {
    width: "100%",
    left: "0",
    rigth: "0",
    top: "0",
    bottom: "0",
    position: "fixed",
    display: "flex",
    backgroundColor: "rgba(0,0,0,0.5)",
    alignItems: "center",
    justifyContent: "center",
  },
  modalContent: {
    borderRadius: "10%",
    aspectRatio: "1/1",
    padding: "25px",
    width: "450px",
    backgroundColor: "rgba(225,225,225,0.9)",
  },
  list: {
    lineHeight: "1.7em",
  },
};

export default NumberModal;
