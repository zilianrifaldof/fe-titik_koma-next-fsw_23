import axios from "axios";
import style from "../../styles/Dashboard.module.css";
import Dashboard_column_left from "../../components/Dashboard_column_left";
import Dashboard_column_right from "../../components/Dashboard_column_right";
import RoomList from "../../components/RoomList";
import WelcomeMessage from "../../components/WelcomeMessage";
import { useDispatch, useSelector } from "react-redux";
import { setSinglePlayerTrue } from "../../share/redux/authSlice";
import SingleRoom from "../../components/CreateRoom/SinglePlayerRoom";
import { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import {
  setModalRoomClickedFalse,
  setModalRoomClickedTrue,
} from "../../share/redux/authSlice";

let pageNumber;

const Dashboard = (props) => {
  const { gameLists } = props;
  const router = useRouter()
  
  const currentPage = router.query.pageNumber || 1;
 
  // const onClickNext = (pageNumber) => {

  //   if(pageNumber>=1){
  //   router.push({
  //     pathname: router.pathname,
  //     query: { pageNumber },
  //   }, undefined, { scroll : false, shallow : false });
    
  // }
  // };


  const dispatch = useDispatch();//added to activate single player modal
  const stateModal = useSelector((state) => state.stateModal);
  
  const [buttonNext, SetButtonNext] = useState(1);
  const [buttonPevious, setPreviousbutton] = useState(1);
  const [buttonNextTrue, SetButtonNextTrue] = useState(false);
  const [buttonPeviousTrue, setPreviousbuttonTrue] = useState(false);
  
  const [gameListState, setGameListState] = useState(gameLists);



//   const fetchPosts  = () => {
   
//     if(buttonNextTrue === true){
//       axios
//       .get(
//         (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
//           `/game/list?pageNumber=${buttonNext}`
//       )
//       .then((res) => {
//         console.log("next 2", buttonPevious)
      
//         const gamelist = res.data;
//        setGameListState(gamelist)
//        gameLists = gameListState
//        console.log(gameLists)
//       console.log(gameLists)
//       setPreviousbutton(buttonNext)
     
//         // console.log(gamelist);
//       })
//       .catch((err) => console.error(err));
//     }
//     else if(buttonPeviousTrue==true){
//        console.log("masuk back")
//       if(buttonPevious>0){
//         console.log("back", buttonPevious)
//         axios
//         .get(
//           (process.env.REACT_APP_BE_URL || "http://localhost:5000") +
//             `/game/list?pageNumber=${buttonNext}`
//         )
//         .then((res) => {
//           console.log("masuk axios 2")
//           const gamelist = res.data;
//          setGameListState(gamelist)
// SetButtonNext(buttonPevious)
//          gameLists = gameListState
       
//           // console.log(gamelist);
//         })
//         .catch((err) => console.error(err));
        
//     }
//     }
//     setPreviousbuttonTrue(false)
//     SetButtonNextTrue(false)
//   };

  // useEffect(() => {
  //   console.log("masuk pagination")
  //   fetchPosts();
  //   // console.log(cookies.accessToken);
  // }, [buttonNext,buttonPevious,gameListState,gameLists]);
  const handleSingle = () => {
    dispatch(setSinglePlayerTrue());
  }

  return (
    <>
      <div className={style.main + " " + "fluid-container"}>
        <div className={style.column + " " + "row"}>
          <Dashboard_column_left></Dashboard_column_left>
          <div className={style.container + " " + "col-md-7"}>
            <div className={style.column_mid}>
              <WelcomeMessage></WelcomeMessage>
              <div className="row mt-5 section_2">
                <div className="col-md-6">
                  <div className={style.quick_play}>
                    <i class="bi bi-caret-right-fill"></i>
                    <span>Quick Play</span>
                  </div>
                  <div className={style.quick_play_box}>
                    <div className={style.card_box}>
                      <h5>vs computer</h5>
                      <button className="btn btn-sm btn-danger" onClick={handleSingle}>
                        Play Now
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col-md-1"></div>
                <div className="col-md-5">
                  <div className={style.score_rule}>Score Rules</div>
                  <div className={style.score_rule_wrapper}>
                    <div className="row">
                      <div className="col-md-3">
                        <div className={style.score_rule_box + " " + "mb-3"}>
                          + 1
                        </div>
                        <div className={style.score_rule_box}>+ 2</div>
                      </div>
                      <div className="col-md-9">
                        <div className={style.score_rule_text + " " + "mb-3"}>
                          Win vs computer
                        </div>
                        <div className={style.score_rule_text}>
                          Win vs player
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <div className={style.bg_hand}></div>
                <div className={style.column_mid +" "+ "col-md-12"}>
                  <RoomList gameListsProps = { gameLists }></RoomList>
        
                </div>
           
          </div>
       
          <Dashboard_column_right></Dashboard_column_right>
          <SingleRoom />
        </div>
      </div>
    </>
  );
};

export default Dashboard;
 
export async function getServerSideProps({req,res}) {


  //PAGINATION WITH CONTEXT QUERY TANPA REQ RES COOKIES 
  // const resGameList = await fetch(`https://games-website-titikkoma-be-dev.herokuapp.com/game/list?pageNumber=${context?.query?.pageNumber || 1}`);
 
  //GET ALL KRNA ADA REQ, RES COOKIES
  const resGameList = await fetch(`https://games-website-titikkoma-be-dev.herokuapp.com/game/list`); 
  const gameLists = await resGameList.json();
  const cookies = getCookie('accessToken',  {req, res} )

  if(!cookies) {
    return {
      redirect: {
        destination: '/login'
      }
    }
  }
  
  return {
    props: {
      gameLists,
      page: 1,
    }
  }

    // const gameLists = await axios
  // .get(
  //   `https://games-website-titikkoma-be-dev.herokuapp.com/game/list?pageNumber=${
  //     context?.query?.pageNumber || 1}`
  // )
  // .then((res) => {
  //   console.log(res.config.url);
  //   return res.data;
  // })
  // .catch((err) => []);
}