import axios from "axios";
import Link from 'next/link';
import { useRouter } from 'next/router';
import { React, useState } from "react";

function Forgot() {
    
    const [values, setValues] = useState({});
    const router = useRouter();   

    const handleChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        axios
        .post("https://games-website-titikkoma-be-dev.herokuapp.com/user/forgot-password", values)
        // .post("http://localhost:5000/user/forgot-password", values)        
        .then((res) => {        
            const data = res.data;
            console.log(res);        
            if (res.status === 200) {
                alert(data.message);               
                router.push("/login");  
            }       
            })
        .catch((err) => {
            const data = err.response;          
            if (data.status === 400) {
                alert(data.data.message);         
            } else {
                alert('invalid Value');         
            }     
        });
    };

    return <div>
        <section className="vh-100" style={{backgroundColor: "#eee"}}>
            <div className="container h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                <div className="col-lg-4 col-xl-4">                  
                    <div className="card text-black" style={{ borderRadius: "30px" }}>
                        <div className="card-body p-md-5">
                            <div className="row justify-content-center">
                                <div className="col-md-12 col-lg-12 col-xl-12 order-2 order-lg-1">
                                    <p className="text-center h4 fw-bold mb-1 mx-1 mx-md-4 mt-1">Forgot Password</p>
                                    <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">We Are Sorry For The Inconvenience, Please Input Your Email To Have A New Password</p>
                                    <form className="mx-1 mx-md-4" onSubmit={handleSubmit}>                                     
                                        <div className="d-flex flex-row align-items-center mb-4">                                   
                                            <div className="form-outline flex-fill mb-0">
                                            <input type="email" id="email" name="email"  onChange={handleChange}
                                                className="form-control" placeholder="Your Email as Username"/>                                 
                                            </div>
                                        </div>                                       
                                        <div className="d-flex flex-row align-items-center mb-4">
                                            <button className="btn btn-md flex-fill" type="submit"
                                            style={{backgroundColor: "#fd7e14", borderColor: "#fd7e14"}}>Submit
                                            </button>
                                        </div> 
                                        {/* <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">- or Login With -</p>                                           */}
                                    </form>
                                    <p className="text-center fw-light mb-3 mx-1 mx-md-4 mt-2">Already Remember Your Password ?&nbsp;
                                        <Link href="/login">                                                        
                                            <a style={{ color: "black", textDecoration: "none" }}><b>Login Now</b></a>   
                                        </Link>                                           
                                    </p>
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>
  }
  
  export default Forgot;