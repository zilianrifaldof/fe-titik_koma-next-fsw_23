import axios from "axios";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useState, useEffect } from "react";
import { useRouter } from 'next/router';
import style from "../../styles/Dashboard.module.css";
import passwordStyle from "../../styles/Password.module.css";
import profileStyle from '../../styles/Profile.module.css';
import Dashboard_column_left from '../../components/Dashboard_column_left';
import Dashboard_column_right from '../../components/Dashboard_column_right';
import WelcomeMessage from "../../components/WelcomeMessage";

const index = (props) => {
    const [users, setUsers] = useState({});
    const [values, setValues] = useState([]);
    const cookies = getCookies();
    const [message, setMessage] = useState(false);

    const enableForm = () => {
        const myInput = document.getElementById("message");
        const inputs = document.querySelectorAll("input");
        const buttonSubmit = document.getElementById("submit");
        const buttonEdit = document.getElementById("enable_submit");
        const messageDom = document.getElementById("messageDom");
        inputs[0].removeAttribute("disabled");
        inputs[1].removeAttribute("disabled");
        buttonSubmit.classList.remove("d-none");
        buttonEdit.classList.add("d-none");
        messageDom.classList.add("d-none");
        myInput.classList.remove("d-none");
        console.log(message);
    };

    if(message) {
      const inputs = document.querySelectorAll("input");
      const buttonSubmit = document.getElementById("submit");
      const buttonEdit = document.getElementById("enable_submit");
      const messageDom = document.getElementById("messageDom");
      inputs[0].setAttribute("disabled", true);
      inputs[1].setAttribute("disabled", true);
      buttonSubmit.classList.add("d-none");
      buttonEdit.classList.remove("d-none");
      messageDom.classList.remove("d-none");
      inputs[0].value = null;
      inputs[1].value = null;
      setMessage(false);
    };

    const handleChange = (e) => {
        setValues({ ...values, password: password.value });
        const myInput = document.getElementById("password");
        const letter = document.getElementById("letter");
        const capital = document.getElementById("capital");
        const number = document.getElementById("number");
        const specialChar = document.getElementById("specialChar");
        const length = document.getElementById("length");
        const message1 = document.getElementById("message1");
        const message2 = document.getElementById("message2");
        message1.innerHTML = "";
        message2.innerHTML = "";
        // Validate lowercase letters
        const lowerCaseLetters = /[a-z]/g;
        if(myInput.value.match(lowerCaseLetters)) {  
            letter.classList.remove(passwordStyle.invalid);
            letter.classList.add(passwordStyle.valid);
        } else {
            letter.classList.remove(passwordStyle.valid);
            letter.classList.add(passwordStyle.invalid);
        }
        // Validate capital letters
        const upperCaseLetters = /[A-Z]/g;
        if(myInput.value.match(upperCaseLetters)) {  
            capital.classList.remove(passwordStyle.invalid);
            capital.classList.add(passwordStyle.valid);
        } else {
            capital.classList.remove(passwordStyle.valid);
            capital.classList.add(passwordStyle.invalid);
        }
        // Validate numbers
        const numbers = /[0-9]/g;
        if(myInput.value.match(numbers)) {  
            number.classList.remove(passwordStyle.invalid);
            number.classList.add(passwordStyle.valid);
        } else {
            number.classList.remove(passwordStyle.valid);
            number.classList.add(passwordStyle.invalid);
        }
        // Validate character
        const specialChars = /[!@#$%^&*]/g;
        if(myInput.value.match(specialChars)) {  
            specialChar.classList.remove(passwordStyle.invalid);
            specialChar.classList.add(passwordStyle.valid);
        } else {
            specialChar.classList.remove(passwordStyle.valid);
            specialChar.classList.add(passwordStyle.invalid);
        }
        // Validate length
        if(myInput.value.length >= 8) {
            length.classList.remove(passwordStyle.invalid);
            length.classList.add(passwordStyle.valid);
        } else {
            length.classList.remove(passwordStyle.valid);
            length.classList.add(passwordStyle.invalid);
        }
    };

    const handleSubmit = (e) => {
        const myInput = document.getElementById("password");
        const pwd1 = document.getElementById("password").value;  
        const pwd2 = document.getElementById("password2").value;

        const upperCaseLetters = /[A-Z]/g;
        const lowerCaseLetters = /[a-z]/g;
        const numbers = /[0-9]/g;
        const specialChars = /[!@#$%^&*]/g;
        
        //check empty password field  
        if(pwd1 == "" || pwd1 == undefined) {  
            document.getElementById("message1").innerHTML = "** Fill the password please!";  
            return false;  
        }

        //check empty confirm password field  
        if(pwd2 == "" || pwd2 == undefined) {  
            document.getElementById("message2").innerHTML = "** Enter the password please!";  
            return false;  
        }

        if(pwd1 !== pwd2) {  
            document.getElementById("message2").innerHTML = "** Passwords are not same!";
            return false;  
        } else if(!pwd1.match(specialChars) || !pwd1.match(numbers) || !pwd1.match(lowerCaseLetters) || !pwd1.match(upperCaseLetters) || pwd1.length < 8) {
            // Validate form
            // document.getElementById("message3").innerHTML = "** not numbers!";
            // console.log(pwd1.length < 8)
            return false
        } else {  
            // alert ("Your password created successfully");
            e.preventDefault();
            axios
            .put(
                (process.env.REACT_APP_BE_URL || "https://games-website-titikkoma-be-dev.herokuapp.com") +
                `/user/changePassword/${cookies.userId}`,
                values,
                {
                headers: { Authorization: `Bearer ${cookies.accessToken}` },
                }
            )
            .then((res) => setMessage(true))
            .catch((err) => {
                alert("Invalid data, check your form");
            });
        }

    };

  return (
    <>
        <div className={style.main}>
            <div className={style.column +" "+ "row"}>
                <Dashboard_column_left></Dashboard_column_left>
                <div className={style.column_mid +" "+ "col-md-7"}>
                  <WelcomeMessage></WelcomeMessage>
                  <div className="row mt-5 profile_section_2">
                    <div className="col-md-6">
                      <div className={profileStyle.profile_title}>
                        <i class="bi bi-caret-right-fill"></i>
                        <span>Password</span>
                      </div>
                      <div id="messageDom" className="d-none">
                        <div class="alert alert-success" role="alert">
                          Success.
                        </div>
                      </div>
                      <div className={profileStyle.profile_box}>
                        {/* <form id="fPassword"> */}
                        <div className={profileStyle.profile_fullname +" "+ "mb-3"}>
                          <label for="password" class="form-label">New password</label>
                          <input type="password" name="password" class="form-control" id="password" placeholder="New password" autocomplete="off" onChange={handleChange} required disabled/>
                          <div id = "message1" style={{color:"red"}}> </div>
                        </div>
                        <div className={profileStyle.profile_email +" "+ "mb-3"}>
                          <label for="password2" class="form-label">New password confirmation</label>
                          <input type="password" name="password2" class="form-control" id="password2" placeholder="New password confirmation" autocomplete="off" onChange={handleChange} required disabled/>
                          <div id = "message2" style={{color:"red"}}> </div>
                        </div>
                        <div className="my-4">
                          <button id="submit" className="btn btn-sm btn-danger d-none" onClick={handleSubmit}>Change password Now</button>
                        </div>
                        <div className="my-4">
                          <button id="enable_submit" class="btn btn-sm btn-danger" onClick={enableForm}>Enable form</button>
                        </div>
                        {/* </form> */}
                        <div id="message" className="d-none">
                            <div id="message3"></div>
                            <h5>Password must contain the following:</h5>
                            <p id="letter" className={passwordStyle.invalid}>A <b>lowercase</b> letter</p>
                            <p id="capital" className={passwordStyle.invalid}>A <b>capital (uppercase)</b> letter</p>
                            <p id="number" className={passwordStyle.invalid}>A <b>number</b></p>
                            <p id="specialChar" className={passwordStyle.invalid}>A <b>special characters</b></p>
                            <p id="length" className={passwordStyle.invalid}>Minimum <b>8 characters or more</b></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <Dashboard_column_right></Dashboard_column_right>
            </div>
        </div>
    </>
  )
}

export default index;

export async function getServerSideProps({ req, res }) {
    const cookies = getCookies({ req, res })
  
    if(!cookies.accessToken) {
      return {
        redirect: {
          destination: '/login'
        }
      }
    }
    
    return {
      props: {}
    }
  }
